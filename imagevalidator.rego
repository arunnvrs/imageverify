package imagevalidator

default allow = false

allow {

	input.critical.type == "cosign container image signature"	
}
